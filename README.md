## LIGHT SHELL GENERAL FAQ


### Why these arent upstreamed into GNOME official light style ?
Unfortunately we cant come into agreement with GNOME dev. Light Shell aims to be more coherent light aestethic (full light theme) compared to official light style (partial light theme) and the dev decided to keep it that way.


### Some applications still dark
Light Shell only affect GNOME Shell, this is intentional to give more flexibility. if you want applications to be all light as well you need to enforce gsettings color scheme to prefer-light:
```
$ gsettings set org.gnome.desktop.interface color-scheme prefer-light
```
to revert, set gsettings color scheme back to default:
```
$ gsettings set org.gnome.desktop.interface color-scheme default
```


### Gahh, my eyesss..
While these arent professional grade, we do our best to put attention in design to make it comfortable to use. just like the dark theme are carefully designed to not just plain black to avoid causing diseases like astigmatism.


### When is GNOME "x" release ?
No promise or certain release fixed set, everything just happen spontaneously. but we try to do our best to maintain and made quality releases. you too can help make this project better, dont hesitate to open issue, comment and fork.


### These would be nice if (insert your feature here)
The scope of this project is only to make identic light counterpart of GNOME Shell dark with minimal changes. you can make your own custom lightshell version by following tutorial included in Light Shell gitlab page.


### I heard light theme is this and that compared to dark theme
Its just happened that light design enthusiast is a rare people but in reality both light and dark theme is equal and had their own share of advantages & disadvantages.

it's perfectly fine to choose either or both. ignore all the myth, just follow your heart which suits you best & enjoy your life.


<div align="center">

best regards

~ dikasp ~

<img src=/dppic.png>

[![button](/button.svg)](https://gravatar.com/dikasetyaprayogi)

</div>
